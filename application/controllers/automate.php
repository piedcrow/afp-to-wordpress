<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of reader
 *
 * @author Martian
 */
class automate extends CI_Controller{

    
    /** 
     * The Constructor
     */
    public function __construct() {
        parent::__construct();
        if ($this->input->is_cli_request()) {
            # code...
        }
        else{
            redirect('my404','refresh');
        }
    }
    
    /*
     * The default
     */
    public function index(){

        $this->scan();
        $this->fetchFeed();
        $this->sort();
        $this->push();
    }

    /*
    * Use the simplexml library to get post properties from the feed
    */
    public function fetchFeed(){
        $categories = $this->afp_model->getCategories();
        foreach ($categories as $category) {
            $this->processFeed($category);
        }
    }

	/*
	* Use the simplexml library to get post properties from the feed
	*/
    public function processFeed($category){
        $path = AFP_FEED.$category['name'].'/';
        print_r(PHP_EOL.'** Handling feed under '.$category['name'].'.'.PHP_EOL);
        if ($feed = file_get_contents($path.'index.xml')) {
            $this->load->library('simplexml');
            $data = $this->simplexml->xml_parseObj($feed);
            $items = $data->NewsItem->NewsComponent->NewsComponent;
            // $lastref = $this->afp_model->lastRef($category['id']);
            $articles = array();
            $article['category'] = $category['id'];
            foreach ($items as $item) {
                $article['post_ref'] = $item->NewsItemRef->attributes->NewsItem;
                // if($article['post_ref'] == $lastref){
                //     break;
                // }
                $dup = $this->afp_model->checkRef($article['category'],$article['post_ref']);
                if ($dup == 0) {
                    $article['post_title'] = stripcslashes($item->NewsLines->HeadLine);
                    array_push($articles, $article);
                }
            }
            $articles = array_reverse($articles);
            if(empty($articles)){
                print_r(PHP_EOL.'- No new posts to log!'.PHP_EOL);
            }
            else{  
                $this->afp_model->logArticles($articles);
                print_r('- '.PHP_EOL.count($articles).' articles have been logged!'.PHP_EOL);
            } 
        }
        print_r(PHP_EOL);  	
    }

    /*
    * Use the simplexml library to push to wp
    */
    public function push(){
        $outlets =  $this->afp_model->autoOutlets();
        if (!empty($outlets)) {
            $this->load->library('simplerpc'); 
            foreach ($outlets as $r) {
                $cats = $this->afp_model->postCats($r['id']);
                if ($cats) {
                    $allcats = array();
                    foreach ($cats as $value) {
                        array_push($allcats, $value['category']);
                    }
                    $unposted = $this->afp_model->unPosted($allcats);
                    if (!empty($unposted)) {
                        $r['url'] = stripslashes($r['url'].'/xmlrpc.php');
                        $links = array();
                        $cat = '';
                        foreach ($unposted as $f) {
                            $dup = intval($this->afp_model->checkPost($f['id'],$r['id']));
                            if ($dup == 0) {
                                $cat = $f['cat'];
                                $links[$f['id']] = $f['post_ref'];
                            }

                        }
                        if (!empty($links)) {
                            $this->simplerpc->fetchDetails($cat, $links, $r);
                        }
                        else{
                            print_r(PHP_EOL.'- Available articles already published on '.$r['url'].PHP_EOL);
                        }                        
                    }
                    else{
                        print_r('- '.PHP_EOL.$r['url'].' has no unpublished articles!'.PHP_EOL);
                    }
                }
                else{
                    print_r('- '.PHP_EOL.$r['url'].' has no categories set for auto publish!'.PHP_EOL);
                }
            }
        }
        else{
            print_r('- '.PHP_EOL.'Cant find auto publish outlets!'.PHP_EOL);
        }
    
    }

    /*
    * Scan feed categories
    */
    public function scan(){
        print_r(PHP_EOL.'** Scan for new feed categories.'.PHP_EOL);
        $cluster = array();
        $categories = array_values($this->afp_model->getCategories());
        foreach ($categories as $category) {
            array_push($cluster, $category['name']);
        }
        $cats = scandir(AFP_FEED);
        foreach ($cats as $i => $cat) {
            if (($cat == '.') || ($cat == '..')) {
                unset($cats[$i]);
            }
        }
        if (empty($categories)) {
            $this->afp_model->newCategory($cats);
            print_r('- '.PHP_EOL.count($cats).' have been added! '.PHP_EOL);
        }
        else{
            $diffs = array_diff($cats, $cluster);
            if(!empty($diffs)){
                $this->afp_model->newCategory($diffs);
                print_r('- '.PHP_EOL.count($diffs).' have been added! '.PHP_EOL);
            }
            else{
                print_r(PHP_EOL.'- No new categories in the ftp dump directory! '.PHP_EOL);
            }
            $diff = array_diff($cluster, $cats);
            if(!empty($diff)){
                $this->afp_model->deleteCategory($diff);
                print_r('- '.PHP_EOL.count($diff).' have been removed! '.PHP_EOL);
            }
        }
    }

    /*
    * sort refs
    */
    public function sort(){
        print_r(PHP_EOL.'** Fixing updated faulty refs.'.PHP_EOL);
        $faulty = $this->afp_model->faultyRefs();
        $count = 0;
        foreach ($faulty as $f) {
            $path = AFP_FEED.$f['name'].'/'.$f['post_ref'];
            if (@file_get_contents($path)) {
                $this->afp_model->markRef($f['id'],3);
                $count++;
            }
            // else{
            //     print_r(PHP_EOL.$f['id'].' has no update yet! '.PHP_EOL);
            // }
        }
        print_r('- '.PHP_EOL.$count.' faulty refs have been updated! '.PHP_EOL);
        // $olds = $this->afp_model->oldRefs();
        // $count = 0;
        // foreach ($olds as $o) {
        //     $this->afp_model->delRef($o['id']);
        //     $count++;
        // }
        // print_r($count.' old refs have been deleted! '.PHP_EOL);


    }

    /*
    * Create archives 
    */
    public function archive(){
        // var_dump(AFP_FEED);
    }

    /*
    * Run tests
    */
    public function test(){
        print_r($this->afp_model->checkpost(125,2));
    }

}