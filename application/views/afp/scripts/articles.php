<link href="<?php echo base_url()?>flatlab/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="<?php echo base_url()?>flatlab/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url()?>flatlab/assets/data-tables/DT_bootstrap.css" />
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>flatlab/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>flatlab/assets/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
  	var articles = '';
  	$('table.table').each(function(index, el) {
  		$(this).dataTable( {
          "aaSorting": [[ 4, "desc" ]]
        });
  	});	
	$('.push-articles').each(function(index, el) {

		$(this).submit(function(event){
			/* Act on the event */
			articles = $(this).serializeArray();
			$('.post-articles').bind('submit', function(event) {
				
				/* Act on the event */
				var outlets = $(this).serializeArray();
				
				$('.posting-articles').each(function(index, el) {
					$(this).find('.modal-title').html('Waiting for response ..');
					$(this).find('.modal-body').html('<p>This may take long, do not close this window yet!</p>');
					$(this).find('.modal-footer').html('<button class="btn btn-info " type="reset" data-dismiss="modal">Close</button>');
				});

				$.post('<?php echo base_url("manage/post") ?>', {articles:articles, outlets:outlets}, function(data, textStatus, xhr) {
					console.log(data);
					$('.posting-articles').each(function(index, el) {
						$(this).find('.modal-title').html('Response');
						$(this).find('.modal-body').html('<pre>'+data+'</pre>');
						$(this).find('.modal-footer').html('<button class="btn btn-info reload" type="reset">Ok</button>');
					});
				});

				event.preventDefault();
			});
			event.preventDefault();
		});
	});	 

	$('body').on('click', '.reload', function(event) {
		event.preventDefault();
		/* Act on the event */
		location.reload();
	});

  });
</script>