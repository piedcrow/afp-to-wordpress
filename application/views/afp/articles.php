<?php
	$new = $revised = array();
	foreach ($articles as $article) {
		if ($article['status'] == 0) {
			array_push($new, $article);
		}
		else if ($article['status'] == 3) {
			array_push($revised, $article);
		}
	}
	$links = $items = $posted = array();
	foreach ($posts as $p) {
		$items[$p['article_id']][] = $p['outlet_id'];
		$links[$p['article_id']][$p['outlet_id']] = $p['post_id']; 
		unset($p['outlet_id']); unset($p['post_id']);
		if (!in_array($p['article_id'], array_keys($posted))) {
			$posted[$p['article_id']] = $p;
		}
	}

?>
<div class="alert alert-success alert-block fade in">
  <button data-dismiss="alert" class="close close-sm" type="button">
      <i class="fa fa-times"></i>
  </button>
  <h4>
      <i class="fa fa-ok-sign"></i>
      This category has <?php echo(count($new) + count($revised)+count($posted))?> articles.
  </h4>
  <p>The articles have been grouped for easier handling.</p>
</div>
<form class="push-articles" method="post" action="">
	<?php if (!empty($new)) :?>
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
		          <header class="panel-heading">
		              New Articles <span class="label label-default"><?php echo count($new);?></span>
		          </header>
		          <div class="panel-body">
		              <div class="adv-table">
		                  <table  class="display table table-bordered table-striped">
		                      <thead>
			                      <tr>
			                      	  <th>Logged</th>
			                          <th>Article Name</th>
			                          <th>Preview</th>
			                          <th>Select</th>
			                      </tr>
			                  </thead>
		                      <tbody>
			                      <?php foreach ($new as $n): ?>
			                      <tr>
			                      	  <td><?php echo(date('d.M.Y',$n['date']))?></td>	
			                          <td><?php echo($n['post_title'])?></td>
			                          <td></td>
			                          <td><input type="checkbox" name="articles" value="<?php echo($n['id'])?>"></td>
			                      </tr>		
			                      <?php endforeach;?>
		                      </tbody>
		                      <tfoot>
			                      <tr>
			                      	  <th></th>
			                          <th></th>
			                          <th colspan="2"><button type="submit" class="btn btn-success" data-toggle="modal" href="#myModal">Post</button></th>
			                      </tr>
		                      </tfoot>
		                  </table>                 
	                  </div>
		          </div>
		    </section>
		</div>
	</div>
	<?php endif; ?>
</form>		
<form class="push-articles" method="post" action="">
	<?php if (!empty($posts)) :?>
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
		          <header class="panel-heading">
		              Posted Articles <span class="label label-default"><?php echo count($posted);?></span>
		          </header>
		          <div class="panel-body">
		              <div class="adv-table">
	                    <table  class="display table table-bordered table-striped">
	                      <thead>
		                      <tr>
		                      	  <th>Posted</th>
		                          <th>Article Name</th>
		                          <th>Outlets Posted</th>
		                          <th></th>
		                      </tr>
	                      </thead>
	                      <tbody>
		                      <?php foreach ($posted as $p): ?>
		                      <tr>
		                      	  <td><?php echo(date('d.M.Y',$p['date']))?></td>	
		                          <td><?php echo($p['post_title'])?></td>
		                          <td><?php foreach ($items[$p['article_id']] as $blog): echo(anchor($outlets[$blog]['url'].'?p='.$links[$p['article_id']][$blog], $outlets[$blog]['name'], array('target' => '_blank')).', '); endforeach;?> </td>
		                          <td><input type="checkbox" name="articles" value="<?php echo($p['id'])?>"></td>
		                      </tr>		
		                      <?php endforeach;?>
	                      </tbody>
	                      <tfoot>
		                      <tr>
		                      	  <th></th>
		                          <th></th>
		                          <th></th>
		                          <th><button type="submit" class="btn btn-success" data-toggle="modal" href="#myModal">Post</button></th>
		                      </tr>
	                      </tfoot>
	                    </table>                    
	                </div>
		          </div>
		    </section>
		</div>
	</div>
	<?php endif; ?>
</form>
<form class="push-articles" method="post" action="">
	<?php if (!empty($revised)) :?>
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
		          <header class="panel-heading">
		              Revised Articles <span class="label label-default"><?php echo count($revised);?></span>
		          </header>
		          <div class="panel-body">
		              <div class="adv-table">
	                    <table  class="display table table-bordered table-striped">
	                      <thead>
		                      <tr>
		                      	  <th>Logged</th>
		                          <th>Article Name</th>
		                          <th></th>
		                      </tr>
	                      </thead>
	                      <tbody>
		                      <?php foreach ($revised as $r): ?>
		                      <tr>
		                      	  <td><?php echo(date('d.M.Y',$r['date']))?></td>	
		                          <td><?php echo($r['post_title'])?></td>
		                          <td><input type="checkbox" name="articles" value="<?php echo($r['id'])?>"></td>
		                      </tr>		
		                      <?php endforeach;?>
	                      </tbody>
	                      <tfoot>
		                      <tr>
		                      	  <th></th>
		                          <th></th>
		                          <th><button type="submit" class="btn btn-success" data-toggle="modal" href="#myModal">Post</button></th>
		                      </tr>
		                   </tfoot>
	                    </table>                    
	                </div>
		          </div>
		    </section>
		</div>
	</div>
	<?php endif; ?>
</form>
<div class="panel-body">
	<div class="modal fade posting-articles" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
          <div class="modal-content">
          	<form class="post-articles" method="post" action="">
          	  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Select Outlets</h4>
              </div>
              <div class="modal-body">
				<p>Select where to post the selected articles.</p>
				<div class="row checkbox">
				  <?php
					foreach ($outlets as $o) { ?>
						<?php if ($o['publish'] != 2): ?>
							<label class="col-lg-6">
	                      		<input name="outlets" value="<?php echo($o["id"])?>" type="checkbox"> <?php echo(anchor($o['url'], $o['name'], array('target' => '_blank')))?>. 
	                        </label>
						<?php endif ?>
					<?php }
				  ?>	
                </div>
              </div>
              <div class="modal-footer">
                  <button class="btn btn-default " type="reset" data-dismiss="modal">Cancel</button>
                  <button class="btn btn-success" type="submit">Post</button>
              </div>	
          	</form>
          </div>
      </div>
    </div>
</div>
