<div class="alert alert-success alert-block fade in">
  <button data-dismiss="alert" class="close close-sm" type="button">
      <i class="fa fa-times"></i>
  </button>
  <h4>
      <i class="fa fa-ok-sign"></i>
      The system has <?php echo(count($outlets))?> outlets.
  </h4>
  <p><strong><?php echo(number_format($count,0,'',','))?></strong> articles have been posted on all outlets so far.</p>
</div>
<div class="panel-group m-bot20" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
          <h4 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                  Outlets List <span class="label label-default"><?php echo count($outlets);?></span>
              </a>
          </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" style="height: auto;">
          <div class="panel-body">
              <?php // var_dump($outlets);?>
              <div class="adv-table">
                <table class="display table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th>Outlet Name</th>
                          <th>Outlet Type</th>
                          <th>Auto Post</th>
                          <th>Url</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php foreach ($outlets as $o): ?>
                      <tr>
                          <td><?php echo(anchor('manage/outlets/'.$o['id'], $o['name']))?></td>   
                          <td style="text-transform:capitalize;"><?php echo($o['type'])?></td>
                          <td><?php echo $publish = ($o['publish'] == 1) ? '<i class="fa fa-check-circle-o"></i>' : '<i class="fa fa-circle-o"></i>' ;?></td>
                          <td><?php echo(anchor($o['url'], ' <i class="fa fa-external-link"></i> ', array('target' => '_blank')))?></td>
                      </tr>     
                      <?php endforeach;?>
                  </tbody>
                  <!-- <tfoot>
                      <tr>
                          <th></th>
                          <th></th>
                          <th><a class="btn btn-success" data-toggle="modal" href="#myModal">Post</a></th>
                      </tr>
                  </tfoot> -->
                </table>                    
              </div>
          </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
          <h4 class="panel-title">
              <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                  Add New Outlet
              </a>
          </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse in" style="height: 0px;">
        <header class="panel-heading">
            Fill in the details below:
        </header>  
        <div class="panel-body">
            <form role="form" class="form-horizontal tasi-form new-outlet" method="post">
                <div class="form-group">
                    <label class="col-lg-2 control-label">Outlet Name</label>
                    <div class="col-lg-10">
                        <input type="text" placeholder="" name="name" class="form-control" required="required">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Outlet Url</label>
                    <div class="col-lg-10">
                        <div class="input-group m-bot15">
                            <span class="input-group-addon">http://</span>
                            <input type="text" class="form-control" name="url" required="required">
                        </div>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Outlet User</label>
                    <div class="col-lg-10">
                        <input type="text" placeholder="" name="user" class="form-control" required="required">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Outlet Password</label>
                    <div class="col-lg-10">
                        <input type="password" placeholder="" name="pass" class="form-control" required="required">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Outlet Type</label>
                    <div class="col-lg-10">
                        <div class="radios">
                            <label class="label_radio r_on" for="radio-01">
                                <input name="type" id="radio-01" value="tv" type="radio"> TV Brand
                            </label>
                            <label class="label_radio r_off" for="radio-02">
                                <input name="type" id="radio-02" value="radio" type="radio"> Radio Brand
                            </label>
                            <label class="label_radio r_off" for="radio-03">
                                <input name="type" id="radio-03" value="other" type="radio"> Other Brand
                            </label>
                        </div>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Auto Publish</label>
                    <div class="col-lg-10">
                        <div class="checkboxes">
                            <label class="label_check c_off" for="checkbox-01">
                                <input name="publish" id="checkbox-01" value="1" type="checkbox"> Allow automatic pushing and publishing of new posts.
                            </label>
                        </div>
                        <div class="col-lg-8 cats hide">
                          <section class="panel">
                            <header class="panel-heading">
                                Categories to auto publish
                            </header>
                            <ul class="list-group">
                                <?php foreach ($categories as $cat) {
                                  ?>
                                    <li class="list-group-item">
                                      <label class="label_check c_off" for="checkbox-02" style="text-transform:capitalize;font-weight:400">
                                        <input name="categories[]" id="checkbox-02" value="<?php echo($cat['id'])?>" type="checkbox"> <?php echo($cat['name'])?>
                                      </label>
                                    </li>  
                                  <?php
                                }?>
                            </ul>
                          </section>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button class="btn btn-danger" type="submit">Add Outlet</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>